from django.shortcuts import render,HttpResponseRedirect
from .forms import PostingForm,CommentForms
from .models import Forum,ForumComment


# Create your views here.

response = {}
def index(request):
    post = Forum.objects.all()
    response['post'] = post
    html = 'wl_forum/wl_forum.html'
    response['form'] = PostingForm()
    posts = Forum.objects.all().order_by('waktu').reverse()
    response['posts'] = posts
    comments = ForumComment.objects.all().order_by('waktu').reverse()
    response['comments'] = comments
    response['commentsForm'] = CommentForms()
    return render(request, html, response)



def postContent(request):
    form =PostingForm(request.POST or None)
    if request.method == 'POST':
        if form.is_valid():
            response['pesan'] = form.cleaned_data.get('content')
            response['tittle'] = form.cleaned_data.get('tittle')
            post = Forum(name=response['tittle'],body=response['pesan'])
            post.save()
            posts = Forum.objects.all().order_by('waktu').reverse()
            response['posts'] = posts
        return HttpResponseRedirect('/forum/')
    else:
        form = PostingForm()
        response['form'] = form
        posts = Forum.objects.all().order_by('waktu').reverse()
        response['posts'] = posts
        return HttpResponseRedirect('/forum/')


def addComment(request,post_id):
    form = CommentForms(request.POST or None)
    if request.method == 'POST':
        if form.is_valid():
            response['pesan'] = form.cleaned_data.get('content')
            theForum = Forum.objects.get(id=post_id)
            post = ForumComment(body=response['pesan'],forum=theForum)
            post.save()
            comments = ForumComment.objects.all().order_by('waktu').reverse()
            response['comments'] = comments
        return HttpResponseRedirect('/forum/')
    else:
        form = CommentForms()
        response['commentsForm'] = form
        comments = ForumComment.objects.all().order_by('waktu').reverse()
        response['comments'] = comments
        return HttpResponseRedirect('/forum/')


def buildComment(request,post_id=None):
    addComment(request,post_id)