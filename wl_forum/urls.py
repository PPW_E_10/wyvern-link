from django.conf.urls import url
from .views import index,postContent,addComment,buildComment

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^postContent', postContent, name='postContent'),
    url(r'^addComment',addComment,name='addComment'),
url(r'^buildComment/(?P<post_id>[0-9]+)/$', addComment, name='buildComment'),


]


