# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-12-12 18:18
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('wl_forum', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='forum',
            name='user_id',
        ),
    ]
