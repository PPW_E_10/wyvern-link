from django import forms


class PostingForm(forms.Form):
    error_messages = {
        'required': 'Tolong isi input ini',
    }
    attrs = {
        'type': 'text',
        'class': 'form-control',
        'placeholder': 'Write something ? '
    }
    tittle = forms.CharField(label='Tittle',required=True)
    content = forms.CharField(label='', required=True, widget=forms.Textarea(attrs=attrs))

class CommentForms(forms.Form):
    error_messages = {
        'required': 'Tolong isi input ini',
    }
    attrs = {
        'type': 'text',
        'class': 'form-control',
        'placeholder': 'Write something ? ',
        'rows' : '3',
        'cols': '3'
    }
    content = forms.CharField(label='Content', required=True,widget=forms.Textarea(attrs=attrs))