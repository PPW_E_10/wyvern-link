from django.db import models

# Create your models here.
class Forum(models.Model):
   # user_id = models.CharField(max_length=200)
    name = models.CharField(max_length=100)
    body = models.CharField(max_length=200)
    waktu = models.DateTimeField(auto_now_add=True)


class ForumComment(models.Model):
    body = models.CharField(max_length=400)
    forum = models.ForeignKey(Forum,on_delete=models.CASCADE)
    waktu = models.DateTimeField(auto_now_add=True)