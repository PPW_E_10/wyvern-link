from django.apps import AppConfig


class WlForumConfig(AppConfig):
    name = 'wl_forum'
