from django.shortcuts import render

# Create your views here.
def index(request):
    html ='dashboard/index.html'
    return render(request, html)
