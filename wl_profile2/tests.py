from django.test import TestCase
from django.test import Client
from .models import Company

def test_profile_url_is_exist(self):
    response = Client().get('/profile/')
    self.assertEqual(response.status_code, 200)

def test_model_can_create_new_message(self):
    #Creating a new activity
    company = Company.objects.create(company_name="kelapa", specialty="consumer goods", yearFounded="2017",description="blablabla",website="hehehe",company_type="hahaha")

    #Retrieving all available activity
    counting_all_company= Company.objects.all().count()
    self.assertEqual(counting_all_company,1)
