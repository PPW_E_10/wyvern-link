from django.db import models

class Company(models.Model):
    company_name = models.CharField(max_length=50)
    specialty = models.CharField(max_length=250)
    yearFounded = models.CharField(max_length=5)
    description = models.CharField(max_length=300)
    website = models.CharField(max_length=300)
    company_type = models.CharField(max_length=300)
