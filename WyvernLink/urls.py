"""WyvernLink URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url,include
from django.contrib import admin
from django.views.generic.base import RedirectView

import wl_profile2.urls as wl_profile
import wl_login.urls as wl_login

import wl_dashboard.urls as wl_dashboard

import wl_forum.urls as wl_forum
#>>>>>>> c9f8164b7e6d1029b3b68ef496fc1284e1179f51

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^login/',include(wl_login, namespace= 'login')),
    url(r'^profile/',include(wl_profile,namespace='profile')),
    url(r'^dashboard/',include(wl_dashboard, namespace= 'dashboard')),
    url(r'^forum/',include(wl_forum,namespace='forum')),

]

#>>>>>>> c9f8164b7e6d1029b3b68ef496fc1284e1179f51
