from django.apps import AppConfig


class WlLoginConfig(AppConfig):
    name = 'wl_login'
